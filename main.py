from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
import selenium
import time
import sys
import pandas as pd

data = pd.read_csv('Book2.csv', error_bad_lines=False)
main = data['LINK']

driver = webdriver.Chrome('./chromedriver.exe')
for i in range(0,3):
    driver.get(main[i])
    try :
        wait = WebDriverWait(driver, 10)
        sendbutton = wait.until(EC.visibility_of_element_located((By.XPATH, '//*[@id="main"]/footer/div[1]/div[3]/button')))
        sendbutton.send_keys(Keys.RETURN)
        time.sleep(3)
    except TimeoutException :
        print ('error')
        continue
driver.close()